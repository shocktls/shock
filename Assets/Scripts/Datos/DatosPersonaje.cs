﻿using UnityEngine;

namespace JuegoShock
{
    [System.Serializable]
    public struct JsonPersonaje
    {
        public int vidaMaxima;
		public int vidaActual;

		public int energiaMaxima;
		public int energiaActual;
    }
}
