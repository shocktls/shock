using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;

public class Controlador_capas:MonoBehaviour
{   
    [SerializeField]    
    private GameObject capaInicial;
    [HideInInspector]
    public GameObject capaActual;
    private void Start()
    {
        capaActual = capaInicial;
    }

    public void CambiarCapa(GameObject _capa){
        capaActual.SetActive(false);
        capaActual = _capa;
        capaActual.SetActive(true);
        Controlador_nivel.Jugador().transform.parent = capaActual.transform;
    }
    public void Cables(bool _estado) {

        float _a = GameObject.Find("Paredes").GetComponent<Tilemap>().color.a;
        StopAllCoroutines();
        if (_estado)
            StartCoroutine(Difuminar(_a));
        else
            StartCoroutine(Mostrar(_a));
    }

    IEnumerator Difuminar(float _a)
    {
        for (float i = (_a * 40); i > 20; i--)
        {
            GameObject.Find("Paredes").GetComponent<Tilemap>().color = new Color(1, 1, 1, i / 40f);
            yield return new WaitForFixedUpdate();
        }
    }
    IEnumerator Mostrar(float _a)
    {
        for (float i = (_a * 40); i <= 40; i++)
        {
            GameObject.Find("Paredes").GetComponent<Tilemap>().color = new Color(1, 1, 1, i / 40f);
            yield return new WaitForFixedUpdate();
        }
    }
}
