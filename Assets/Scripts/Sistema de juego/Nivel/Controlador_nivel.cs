﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_nivel : Administrador_nivel
{

    private Entradas entrada;

    ///
    //asigna valores internos antes de iniciar el juego
    private void Awake()
    {
        entrada = GetComponent<Entradas>();
    }



    ///
    //inicia el juego
    public void Inicializar()
    {
        //genera las instancia para el juego
        InstaciarElementosJuego();

        //inicia los controladores del juego
        IniciarControladores();

        //enciende las entradas del jugador
        entrada.enabled = true;
        camara.enabled = true;
    }


    ///
    //instancia los elementos principales del juegos
    private void InstaciarElementosJuego()
    {
        //genera y actualiza el controlador de shock
        GameObject _shock = Instantiate(prefabShock, puntoIncialPersonaje.position, puntoIncialPersonaje.rotation);
        Destroy(puntoIncialPersonaje.gameObject);
        AsignarPersonaje(_shock);

        //genera y actualiza el controlador de la mira
        GameObject _mira = Instantiate(prefabInteraccion);
        AsignarMira(_mira);
    }



    //inicializa los controladores del juego, funciona como un LateStart()
    private void IniciarControladores()
    {
        controlHUD.Incializar(this);
        controlInteraccion.Iniciar(this);
    }



    public void SalirPersonaje()
    {
        if (PersonajeActual == PersonajeShock) return;
        PersonajeActual.CancelarPosesion();
        PersonajeShock.transform.position = PersonajeActual.PuntoSalida;
        CambiarPersonaje(PersonajeShock.gameObject);
    }



    public void CambiarPersonaje(GameObject _nuevoPersonaje)
    {
        PersonajeActual.Salir();
        ActualizarControladorEntidad(_nuevoPersonaje.GetComponent<Controlador_entidad>());
        PersonajeActual.Entrar();
        controlHUD.QuitarExtra();
        if (_nuevoPersonaje != PersonajeShock.gameObject)
        {
            AsignarPersonajeExtra(_nuevoPersonaje.GetComponent<Controlador_personaje>());
            controlHUD.CargarExtra();
        }

        controlInteraccion.Estado(PersonajeActual.puedeInteractuar);

        Debug.Log("Se a cambiado de personaje correctamente");
    }


    public void BloquearControlJugador()
    {
        entrada.interaccionEntidad = false;
    }



    public void DesbloquearControlJugador()
    {
        entrada.interaccionEntidad = true;
    }



    public void PausarJuego()
    {
        Pausar(!pausa);
        Time.timeScale = pausa ? 0 : 1;
    }
    

    public static void TransferenciaEntidad(GameObject _entidad) {
        if (!GameObject.Find("Controlador de nivel"))
        {
            Debug.LogWarning("No existe un controlador de nivel");
            return;
        }
        if (GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>())
        {
            Controlador_nivel _Nivel = GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>();
            _Nivel.CambiarPersonaje(_entidad);
            return;
        }
        Debug.LogError("El controlador de nivel está mal configurado");
    }
    public static Controlador_interaccion Interactor() {
        if (!GameObject.Find("Controlador de nivel"))
        {
            Debug.LogWarning("No existe un controlador de nivel");
            return null;
        }
        if (GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>())
        {
            Controlador_nivel _Nivel = GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>();
            return _Nivel.controlInteraccion;
        }
        Debug.LogError("El controlador de nivel está mal configurado");
        return null;
    }
    public static Controlador_entidad Jugador() { 
        if (!GameObject.Find("Controlador de nivel"))
        {
            Debug.LogWarning("No existe un controlador de nivel");
            return null;
        }
        if (GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>())
        {
            Controlador_nivel _Nivel = GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>();
            
            return _Nivel.PersonajeActual.GetComponent<Controlador_entidad>();
        }
        Debug.LogError("El controlador de nivel está mal configurado");
        return null;
    }
}
