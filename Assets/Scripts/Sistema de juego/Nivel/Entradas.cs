﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Entradas : MonoBehaviour {

    [HideInInspector]
    public bool interaccionEntidad = true;//¿puede el jugador interactuar con el personaje?

    private Controlador_nivel juego;
    public static Entradas principal;

    private void Awake(){
        juego = GetComponent<Controlador_nivel>();
    }
    private void Start() {
        principal = this;
    }

    public void Interaccion(bool _estado) {
        interaccionEntidad = _estado;
        juego.PersonajeActual.Mover(Vector2.zero);
    }
    private void Update () {
        ControlJuego();

        if(juego.pausa) return; 

        Entidad();
		
    }
    private void FixedUpdate() { 
        if(juego.pausa) return; 
        
        Interaccion();
    }

    private void ControlJuego() { 
		if(Input.GetKeyDown(KeyCode.G)) // pausar el juego
            juego.PausarJuego();
        
    }

    private void Entidad(){
        
        if(!interaccionEntidad) return;

        float _direccionX = Input.GetAxis("Horizontal");
        float _direccionY = Input.GetAxis("Vertical");
        Vector2 _direccion = new Vector2(_direccionX,_direccionY);
        juego.PersonajeActual.Mover(_direccion);
        if(Input.GetKeyDown(KeyCode.E))
        juego.PersonajeActual.Usar();
        personaje();

    }
    private void personaje() 
    {
        if(!juego.PersonajeActual.GetComponent<Controlador_personaje>()) return;

        Controlador_personaje _personaje = juego.PersonajeActual.GetComponent<Controlador_personaje>();
        float _direccionX = Input.GetAxisRaw("Horizontal");
        float _direccionY = Input.GetAxisRaw("Vertical");
        Vector2 _direccion = new Vector2(_direccionX,_direccionY);

        //if(Input.GetKeyDown(KeyCode.LeftControl))
           // _personaje.AccionSegunda(_direccion);

        if(Input.GetKeyDown(KeyCode.Space))
			_personaje.Saltar();

        
    }

    private void Interaccion() {
        if (juego.controlInteraccion.visible)
        {
            juego.controlInteraccion.Apuntar();

            if (Input.GetMouseButton(1))
            {
                juego.controlInteraccion.VerificarEntidad();
                if (Input.GetMouseButton(0))
                    juego.controlInteraccion.CargarRayo();
                else
                    juego.controlInteraccion.CancelarCarga();
            }
            else
                juego.controlInteraccion.DejarVerificarEndidad();
        }
        if(Input.GetKeyDown(KeyCode.Q))
            juego.controlInteraccion.CancelarPosesion();
        
    }

}
