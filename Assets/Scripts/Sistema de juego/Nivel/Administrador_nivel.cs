﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Administrador_nivel : MonoBehaviour {
    //Pd. estas tres variables seran variables privadas en el futuro
    public GameObject prefabShock;//prefab de shock 
    public GameObject prefabInteraccion;//prefab de la mira del juego 
    public Transform puntoIncialPersonaje;//punto inicial de instancia del personaje(Shock) 
    public Controlador_hud controlHUD;// controlador del hud

    public Controlador_camara camara;
    public Administrador_personaje adminPersonajeExtra { get; private set; } // personaje extra que es jugador puede controlar, si es que lo hay, sino es nulo;
    public Controlador_personaje PersonajeShock { get; private set; } // controlador del personaje principal del juego;
    public Controlador_entidad PersonajeActual { get; private set; } // controlador del personaje actual del juego;
    public Controlador_interaccion controlInteraccion{ get; private set; }// controlador de la mira;
    

    public bool pausa { get; private set;} // estado actual del juego(¿en pausa?)

    protected void Pausar(bool _pausa) {
        pausa = _pausa;
        Debug.Log("Se a pausado el juego");
    }

    protected void AsignarPersonaje(GameObject _shock) {
        PersonajeActual = _shock.GetComponent<Controlador_personaje>();
        PersonajeShock = _shock.GetComponent<Controlador_personaje>();
        Debug.Log("Se a asignado un personaje");
    }
	protected void AsignarMira(GameObject _mira) {
        controlInteraccion = _mira.GetComponent<Controlador_interaccion>();
        Debug.Log("Se a asignado la mira");
    }
    protected void AsignarPersonajeExtra(Administrador_personaje _extra) {
        adminPersonajeExtra = _extra;
        Debug.Log("Se a asignado un personaje extra");
    }

    protected void ActualizarControladorEntidad(Controlador_entidad _personaje) {
        PersonajeActual = _personaje;
        Debug.Log("Se a actualizado el controlador del personaje, el personaje actual es " + _personaje.name);
    }
}
