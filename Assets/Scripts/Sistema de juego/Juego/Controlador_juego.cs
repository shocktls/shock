﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Controlador_juego : Administrador_juego {


    private void Start() {
        DontDestroyOnLoad(this);
    }
    public void RecibirNivel(string _nivel) {
        StartCoroutine(CargarNivel(_nivel));
    }

    private IEnumerator CargarNivel(string _nivel) {
        if(SceneManager.GetSceneByName(_nivel)==null) yield break;

        AsyncOperation _nvl= SceneManager.LoadSceneAsync(_nivel);

        yield return _nvl;
        interfazGrafica.CargoNivel();
        CargarNecesarios();
        AsignarNivel();
    }
    private void CargarNecesarios() {
        interfazGrafica.CargarHud();
    }
    private void AsignarNivel() { 

        if(GameObject.Find("Controlador de nivel"))
        nivel = GameObject.Find("Controlador de nivel").GetComponent<Controlador_nivel>();
        nivel.controlHUD = interfazGrafica.hud;
        nivel.Inicializar();
    }

}
