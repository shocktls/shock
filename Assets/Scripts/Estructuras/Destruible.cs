﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(BoxCollider2D))]
public class Destruible : MonoBehaviour
{     
    private enum Nivel
    {
        Nivel_1,
        Nivel_2,
        Nivel_3,
        Nivel_4
    }   

    [SerializeField]
    private Nivel dureza;
    
    private bool habilitado=true;
    private BoxCollider2D caja;

    private void Awake() {
        caja = GetComponent<BoxCollider2D>();
    }

    private float tiempoActual;
    private Transform obj;

    private void OnCollisionEnter2D(Collision2D _obj) { 
        if(_obj.collider.GetComponent<Shock>())
            GetComponent<Animator>().Play("Romper");
    }
    private void OnCollisionStay2D(Collision2D _obj) { 
        if(!habilitado) return;
        obj = _obj.transform;
        Activar();
    }
    private void Activar() {
        TipoNivel(dureza);
    }
    private void TipoNivel(Nivel _nivel) { 
        switch (dureza)
        {
            case Nivel.Nivel_1:
                Nivel1();
                break;
            case Nivel.Nivel_2:
                Nivel2();
                break;
            case Nivel.Nivel_3:
                Nivel3();
                break;
            case Nivel.Nivel_4:
                Nivel4();
                break;
        }
    }

    private void Nivel1() { 
        if(obj.CompareTag("Player"))
			if(obj.position.y>transform.position.y)
                tiempoActual += Time.deltaTime;
		if(tiempoActual>=1)
            Destruir();
    }
    private void Nivel2() { 
        
    }
    private void Nivel3() { 
        
    }
    private void Nivel4() { 
        
    }

    private void Destruir() {
        Cambiar(false);
        StartCoroutine(Espera());
    }
    private void Regenerar() {
        
        Cambiar(true);
    
    }
    private void Cambiar(bool _estado) {
        habilitado = _estado;
        GetComponent<Collider2D>().enabled = _estado;
        GetComponent<SpriteRenderer>().enabled = _estado;
    }
    IEnumerator Espera() {
        yield return new WaitForSeconds(2);
        while (true)
        {   
            
            Collider2D _f = Physics2D.OverlapBox(Utilidades.SumaVectores(transform.position,caja.offset), caja.size*0.9f, 0);
            if (!_f)
            {   
                //yield return new WaitForSeconds(2f);
                Regenerar();
                yield break;
            }
            yield return new WaitForSeconds(0.02f);
        }
        
    }

    private void OnCollisionExit2D(Collision2D _obj) {
        tiempoActual = 0;
        if(_obj.collider.GetComponent<Shock>())
            GetComponent<Animator>().Play("Vacio");
    }
}
