using System.Collections;
using UnityEngine;

public class Puerta : MonoBehaviour, IMecanismo
{
    [SerializeField]
    private bool abierta;
    [SerializeField]
    private float velocidad = 0.2f;

    private bool activando = false;

    public float retraso = 0;

    protected void Start()
    {

        if (!abierta) return;

        GetComponent<SpriteRenderer>().sprite = null;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Animator>().enabled = false;
    }

    public void Activar()
    {

        activando = true;
        if (gameObject.activeInHierarchy)
            StartCoroutine(Realizar());
        else
            Alternativo();
    }
    private void Alternativo() { 
        if(!programado)
            programado = !programado;
        else
        {
            GetComponent<Animator>().Play(activando ? "Abrir" : "Cerrar");
            GetComponent<Animator>().enabled = true;
            GetComponent<Collider2D>().enabled = !activando;
        }
    }
    private bool programado;
    private void OnEnable() { 
        if(programado)
            Alternativo();
    }
    IEnumerator Realizar() {
        if(retraso<0) retraso = 0;
        yield return new WaitForSeconds(retraso);
        GetComponent<Animator>().Play(activando? "Abrir":"Cerrar");
        GetComponent<Animator>().enabled = true;
    }
    //es llamado desde el animator
    public void AbrirPuerta() {
        GetComponent<Collider2D>().enabled = !activando;
    }

    public void Desactivar()
    {
        
        activando = false;
        if(gameObject.activeInHierarchy)
            StartCoroutine(Realizar());
        else
            Alternativo();
    }
}