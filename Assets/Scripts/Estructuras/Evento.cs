﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evento : MonoBehaviour {

    public GameObject[] activarMecanismos;
	public GameObject[] DesactivarMecanismos;

    private void OnTriggerEnter2D(Collider2D _obj)
    { 
		if(_obj.GetComponent<Controlador_entidad>())
			if(_obj.GetComponent<Controlador_entidad>().enPosecion)
            	Realizar();
    }
    private void Realizar() { 
		foreach (GameObject _i in activarMecanismos)
        {   
            if(_i.GetComponent<IMecanismo>()!=null)
            _i.GetComponent<IMecanismo>().Activar();
        }
		foreach (GameObject _i in DesactivarMecanismos)
        {   
            if(_i.GetComponent<IMecanismo>()!=null)
            _i.GetComponent<IMecanismo>().Desactivar();
        }
        Destroy(gameObject);
    }


}
