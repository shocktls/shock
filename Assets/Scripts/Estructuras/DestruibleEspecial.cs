using UnityEngine;

public class DestruibleEspecial : MonoBehaviour
{
    private enum Elemento { 
        sobreEnergia,
        impulso,
        rebote,
        retroceso,
        perforacion,
        araña,
        soldado,
    }
    [SerializeField]
    private Elemento especial;

    private Transform objetivo;

    private void OnTriggerEnter2D(Collider2D _objetivo) {
        objetivo = _objetivo.transform;
        TipoElemto();
    }

    private void TipoElemto() { 
        switch (especial)
        {
            case Elemento.sobreEnergia:
                SobreEnergia();
                break;
            case Elemento.impulso:
                Impulso();
                break;
            case Elemento.rebote:
                Rebote();
                break;
            case Elemento.retroceso:
                Retroceso();
                break;
            case Elemento.perforacion:
                Perforacion( );
                break;
            case Elemento.araña:
                Araña();
                break;
            case Elemento.soldado:
                Soldado();
                break;
        }
    }

    private void SobreEnergia()
    { 

    }
    private void Impulso() 
    { 
        if(objetivo.CompareTag("Player"))
            if(objetivo.GetComponent<Shock>().enImpulso)
                Destruir();
    }
    private void Rebote()
    { 

    }
    private void Retroceso()
    { 

    }
    private void Perforacion()
    { 

    }
    private void Araña()
    { 

    }
    private void Soldado()
    { 

    }

    private void Destruir() {
        Destroy(gameObject);
    }
}