﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour,IMecanismo {

    [SerializeField]
    private bool unaVez=false;
    [SerializeField]
	private bool activado=false;
	[SerializeField]
    private Transform[] recorridos;
    [SerializeField]
    private float velocidad;

	
	private int puntoRecorrido=0;
    private int direccion=1;
    private float lugarActual=0;
    private Vector3 posiAnterior;
    
    void Start() {
		if(recorridos.Length>0)
            if (recorridos[0])
            {
                transform.position = recorridos[0].position;
                posiAnterior = recorridos[0].position;
            }
    }

    void IMecanismo.Activar()
    {	
		if(activado) return;
		activado = true;
        Reiniciar();
    }

    void IMecanismo.Desactivar()
    {	
		if(!activado) return;
        activado = false;

    }
    void OnTriggerEnter2D(Collider2D _obj) {
        if(!gameObject.activeSelf) return;
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        _obj.transform.parent = transform;
    }
	void OnTriggerExit2D(Collider2D _obj) {
        if(!gameObject.activeSelf) return;
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        StartCoroutine(SalirHijo(_obj));
    }

    IEnumerator SalirHijo(Collider2D _obj) {
        yield return new WaitForEndOfFrame();
        _obj.transform.parent = transform.parent;
    }



    private void FixedUpdate()
    {
        if (!activado) return;
        if (recorridos.Length == 0) return;

        lugarActual += Time.deltaTime * velocidad/Vector3.Distance(posiAnterior,recorridos[puntoRecorrido].position);

        transform.position = Vector3.Lerp(posiAnterior, recorridos[puntoRecorrido].position, lugarActual);

        if (lugarActual >= 1)
        {
			puntoRecorrido+=direccion;

            if (puntoRecorrido < 0)
            {
                puntoRecorrido++;
                direccion = 1;
				if(unaVez)
                    activado = false;
            }
            if (puntoRecorrido >= recorridos.Length)
            {
                puntoRecorrido--;
                direccion = -1;
				if(unaVez)
                    activado = false;
            }
            lugarActual = 0;
            Reiniciar();
        }

    }

    private void Reiniciar() {
		posiAnterior = transform.position;
	}


}
