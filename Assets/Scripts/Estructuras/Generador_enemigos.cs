﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador_enemigos : MonoBehaviour {

    [SerializeField]
    private int cantidadEnemigos=1;
    [SerializeField]
    private GameObject prefabEnemigo;
    private GameObject[] enemigoActual;

    [SerializeField]
    private Transform patrullaInicio;
    [SerializeField]
    private Transform patrullaFin;

    private bool inicial=false;

    IEnumerator Start() {
        enemigoActual = new GameObject[cantidadEnemigos];
        for (int i = 0; i < enemigoActual.Length; i++)
        {
            if (!enemigoActual[i])
                Generar(i);
            yield return new WaitForSeconds(1.5f);
        }
        inicial = true;
    }

    private void Generar(int i) { 
        enemigoActual[i] = Instantiate(prefabEnemigo,transform.position,transform.rotation);
        enemigoActual[i].transform.parent = transform;
        if(enemigoActual[i].GetComponent<Enemigo>())
            enemigoActual[i].GetComponent<Araña>().ConfigurarPatrulla(patrullaInicio,patrullaFin);
        StartCoroutine(Mostrar(i));
    }

    IEnumerator Mostrar(int i) {
        for (int _i = 0; _i < 40;_i++)
        {
            enemigoActual[i].transform.Translate(Vector3.up/50);
            //enemigoActual.transform.position = Vector3.Lerp(transform.position,transform.position+Vector3.up,i/100);
            yield return new WaitForSeconds(0.02f);
        }
        enemigoActual[i].GetComponent<Collider2D>().enabled = true;
        enemigoActual[i].GetComponent<Rigidbody2D>().bodyType =RigidbodyType2D.Dynamic;
		
		enemigoActual[i].GetComponent<Araña>().enabled = true;
		
    }

    private void Update() {
        if(inicial)
        for (int i = 0; i < enemigoActual.Length;i++)
            if (!enemigoActual[i])
                Generar(i);
    }

}
