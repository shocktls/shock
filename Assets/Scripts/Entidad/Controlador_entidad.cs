﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_entidad : Administrador_entidad {

    protected virtual void Awake() { 
        if(!centroEntidad)
            centroEntidad = transform;
        if(!puntoSalida)
            puntoSalida = transform;
    }
    
    protected virtual void Start() { 

    }

    public virtual void Usar() { 

    }

    public virtual void Mover(Vector2 _direccion){
	}

    public virtual void Daño(int _daño) {
    }

    public virtual void Destruirse() {

    }

    public virtual void Entrar() { 
        enPosecion = true;
        if (GetComponent<Collider2D>())
        {
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Collider2D>().enabled = true;
        }
    }

    public virtual void Salir() {
        enPosecion = false;
    }

    public virtual void CancelarPosesion()
    {
        Destruirse();
    }

}
