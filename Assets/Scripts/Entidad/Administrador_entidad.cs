﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Administrador_entidad : MonoBehaviour {

    public bool polaridad;
    public bool enPosecion;
    public bool interactuable = true;
    public bool puedeSalir = true;
    public bool puedeInteractuar=true;

    [SerializeField]
    protected Transform centroEntidad;

    public Transform Centro{ 
        get 
        {
            return centroEntidad;
        } 
    }

    [SerializeField]
    protected Transform puntoSalida;

    public Vector3 PuntoSalida{ 
        get 
        {
            return puntoSalida.position;
        } 
    }

    public virtual float TiempoPosesion{ get { return 0.5f; } }
}
