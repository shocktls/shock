using System.Collections;
using UnityEngine.Playables;
using UnityEngine;

public class Generador : Controlador_objeto
{
    [SerializeField]
    private bool temporal;

    [SerializeField]
    private Transform barra;

    [SerializeField]
    private GameObject[] mecanismos;

    [SerializeField]
    private float cargaMaxima;
    [SerializeField]
    private float cargaActual=0;

    [SerializeField]
    private float resistencia;

    private bool activado;

    public override void Entrar() {
        base.Entrar();
        barra.parent.gameObject.SetActive(true);
    }
    public override void Salir() {
        base.Salir();
        barra.parent.gameObject.SetActive(false);
    }

    private void Update() {     
        if(activado) return;
        barra.localScale = new Vector2(cargaActual/cargaMaxima,1);
        if (cargaActual >= cargaMaxima)
        {

            Enviar();
            return;
        }
        if(cargaActual == 0) return;
        cargaActual -= Time.deltaTime * resistencia;
        if (cargaActual < 0)
            cargaActual = 0;
        
    }

    public override void Usar()
    {
        if(!activado)
        cargaActual += resistencia/3;
    }

    private void Enviar() {
        activado = true;
        cargaActual = cargaMaxima;
        if(GetComponent<PlayableDirector>())
            GetComponent<PlayableDirector>().enabled = true;
        
        
        foreach (GameObject _i in mecanismos)
        {   
            if(_i.GetComponent<IMecanismo>()!=null)
            _i.GetComponent<IMecanismo>().Activar();
        }
        barra.GetComponent<SpriteRenderer>().size = new Vector2(cargaActual/cargaMaxima,1);
        if (temporal)
        {
            cargaActual = 0;
            activado = false;
        }
    }
    
}