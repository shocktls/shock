﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Enchufe : Controlador_objeto {
	
	public Transform[] arriba=new Transform[0];
	public Transform[] abajo=new Transform[0];
    public Transform[] derecha=new Transform[0];
	public Transform[] izquierda=new Transform[0];

	[SerializeField]
    private GameObject prefabParticula;
    private Transform particula;

    public float velocidadTransferencia=6;
    private bool cambio;
    private Vector3 posOriginal;
    protected bool transfiriendo;

    protected override void Start() {
        base.Start();
        posOriginal = centroEntidad.position;
    }

    private void Limpiar(Controlador_entidad _entidad) {
		if(!_entidad) return;
        if(!_entidad.gameObject.CompareTag("Objeto"))
            _entidad = null;
    }
    private Transform[] Buscar(Vector2 _direccion) { 
		if(_direccion.x>0)
            return derecha;
		if(_direccion.x<0)
			return izquierda;
		if(_direccion.y>0)
            return arriba;
		if(_direccion.y<0)
            return abajo;
        return new Transform[0];
    }

    public override void Entrar()
    {
        base.Entrar();
        transform.root.GetComponent<Controlador_capas>().Cables(true);
    }
    public override void Salir()
    {
        base.Salir();
        transform.root.GetComponent<Controlador_capas>().Cables(false);
    }

    public override void Mover(Vector2 _direccion) {
		if(_direccion==Vector2.zero) return;
        if(transfiriendo) return;
		if(Buscar(_direccion).Length==0) return;
        if(prefabParticula)
        
        transfiriendo = true;
        puedeSalir = false;
        if(!particula)
                particula = Instantiate(prefabParticula,centroEntidad).transform;
        StartCoroutine(Transferir(_direccion));
        
    }
	
    IEnumerator Transferir(Vector2 _direccion) {
        yield return new WaitForEndOfFrame();
        foreach (Transform i in Buscar(_direccion))
        {
            if (i)
            {	
                

                centroEntidad.position = i.position;
                
				
                
                if (i.childCount != 0 && i.GetChild(0).GetComponent<Controlador_entidad>() && i.gameObject.CompareTag("Objeto"))
                {
                    Cambiar(i.GetChild(0).gameObject);
                    break;
                }
                if (i.GetComponent<Controlador_entidad>() && i.gameObject.CompareTag("Objeto"))
                {
                    Cambiar(i.gameObject);
                    break;
                }

                if(velocidadTransferencia>0)
                yield return new WaitForSeconds(1/velocidadTransferencia);
                else
                {
                    yield return new WaitForSeconds(0);
                    velocidadTransferencia = 0;
                }

            }
        }
        //Salir();
    }

    public virtual void Cambiar(GameObject _entidad) {
        Controlador_nivel.TransferenciaEntidad(_entidad);
        if (particula)
            particula.parent = null;
        particula = null;
        enPosecion = false;
        puedeSalir = true;
        transfiriendo = false;
        centroEntidad.position = posOriginal;
    }

}
