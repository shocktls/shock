﻿using UnityEngine;

public class Foco : Enchufe
{

    protected Animator anim;

    protected override void Start()
    {
        base.Start(); 
        if(puedeInteractuar)
        anim.Play("Roto");
    }

    protected override void Awake() {
        base.Awake();
        anim = GetComponent<Animator>();
    }

    public override void Cambiar(GameObject _entidad) {
        base.Cambiar(_entidad);
        if(!puedeInteractuar)
        anim.Play("Apagado");
    }   

    public override void Salir()
    {
        base.Salir();
        if(transfiriendo) return;
        puedeInteractuar = true;
        interactuable = true;
        anim.Play("Roto");
    }
    public override void Entrar()
    {
        base.Entrar();
        if(!puedeInteractuar)
        anim.Play("Prendido");
    }
}
