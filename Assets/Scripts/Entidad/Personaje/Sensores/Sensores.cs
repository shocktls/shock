using UnityEngine;

public class Sensores {

    private Transform personaje;
    private Vector2 area;
    private Vector2 desfase;

    public Sensores(Transform _personaje) {
        personaje = _personaje;
        if (_personaje.GetComponent<BoxCollider2D>())
        {
            area = _personaje.GetComponent<BoxCollider2D>().size;
            desfase = _personaje.GetComponent<BoxCollider2D>().offset;
        }
        if (_personaje.GetComponent<CircleCollider2D>())
        {
            area = Vector2.one * _personaje.GetComponent<CircleCollider2D>().radius*2;
            desfase = _personaje.GetComponent<CircleCollider2D>().offset;
        }

    }

    public bool EnSuelo() {
        Vector3 _centro = Utilidades.SumaVectores(personaje.position, desfase);
        RaycastHit2D _abajoCentro = Utilidades.Rayo(personaje.position, -personaje.up,area.y/2+0.1f,new ContactFilter2D());
        RaycastHit2D _abajoIzquierda =Utilidades.Rayo(-personaje.right * (area.x/2.1f)+_centro, -personaje.up,area.y/2+0.1f,new ContactFilter2D());
        RaycastHit2D _abajoDerecha =Utilidades.Rayo(personaje.right * (area.x/2.1f)+_centro, -personaje.up,area.y/2+0.1f,new ContactFilter2D());
        
        if(_abajoCentro)
            return true;
        if(_abajoDerecha)
            return true;
        if(_abajoIzquierda)
            return true;
        return false;
    }
}