﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_personaje : Administrador_personaje {

	protected Rigidbody2D cuerpo;

    protected Sensores sensores;

    protected Animator anim;

    protected override void Awake(){
        base.Awake();
        cuerpo = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    protected override void Start() {
        base.Start();
        sensores = new Sensores(transform);
    }
    float fe;
    protected virtual void Update()
    { 
        
       
    }
    protected virtual void FixedUpdate()
    {
        fe -= transform.position.x;
        fe /= Time.deltaTime;
        Animaciones();
        fe = transform.position.x;
    }
    protected virtual void Animaciones() {
        anim.SetFloat("xVelocidad", Mathf.Abs(fe)/velocidad);
        anim.SetFloat("yVelocidad", cuerpo.velocity.y);
    }

    public override void Mover(Vector2 _direccion) {
         cuerpo.velocity = new Vector2(_direccion.x * velocidad, cuerpo.velocity.y);
    }

    public virtual void Saltar(){
        anim.Play("Salto_a");
        if(!sensores.EnSuelo()) return;
		cuerpo.AddForce(Vector2.up*fuerzaSaltoActual);
	}
    public override void Daño(int _daño) {
        vidaActual -= _daño;

		if(vidaActual<=0)
            Destroy(gameObject);
    }
    public virtual void RecibirDaño(int _daño) 
    {   
        if(_daño<0) return;
        vidaActual -= _daño;
        if(vidaActual<0)
            vidaActual = 0;
    }

    //tecla shift
    public virtual void AccionPrimera() { 

    }
    //tecla direccional
    public virtual void AccionSegunda(Vector2 _direccion,bool _estado=true) { 
        
    }
    public override void Destruirse() {
        Destroy(gameObject);
    }

}
