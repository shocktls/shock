﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IA.MaquinaEstado;

public class Araña : Enemigo
{
   
    public float distanciaContacto = 2;
    public Transform puntoRotacion;
    public ContactFilter2D filtroSensores;

    private bool puedeMoverse;
    private bool cambioDireccion;
    private Patrulla patrulla;
    private bool enTransferencia;
    private float direccionAnterior=1;
    private bool enSalto;
    private float anguloGiro;
    private bool enGiro = false;
    private Vector3 puntoAnterior;
    [SerializeField]
    private float tiempoPosesion = 0.2f;

    public override float TiempoPosesion{ get { return tiempoPosesion; } }
    //codigo sucio...  ordenar luego

    protected override void Start() {
        base.Start();
        direccionAnterior = transform.right.x;
        patrulla = new Patrulla(this);
        patrulla.direccion = direccionAnterior;
    }

    protected override void Update()
    {
        base.Update();
        sensores.EnSuelo();
        
        //Debug.Log(transform.up.ToString("f4"));
        if(enTransferencia) return;
        Sensores();
        if(enSalto) return;
        if(enPosecion) return;
        patrulla.Realizar();

        //MoverAraña(cambioDireccion? Vector2.left:Vector2.right);
        //SensoresAraña();
    }


    public override void Mover(Vector2 _direccion)
    {   
        if(enSalto) return;
        if(enTransferencia) return;
        MoverAraña(_direccion);
    }

    private void Sensores()
    {
        float _radio = GetComponent<CircleCollider2D>().radius*2;

        RaycastHit2D _objetoAbajo = Utilidades.Rayo(transform.position, -transform.up, distanciaContacto*_radio,filtroSensores);
        RaycastHit2D _objetoDerecha = Utilidades.Rayo(transform.position, (transform.right) * 1.1f - transform.up, distanciaContacto *_radio * 2,filtroSensores);
        RaycastHit2D _objetoIzquierda = Utilidades.Rayo(transform.position, (-transform.right) * 1.1f - transform.up, distanciaContacto *_radio * 2,filtroSensores);
        puedeMoverse = false;
        if (enGiro) { Girar(_objetoIzquierda, _objetoAbajo, _objetoDerecha, _objetoDerecha.normal); return; }

        
        if (!_objetoAbajo && !_objetoDerecha && !_objetoIzquierda)
        {
            Caer();
            return;
        }
        if (_objetoIzquierda && !_objetoAbajo && !_objetoDerecha)
        {   
            enGiro = true;

            return;
        }
        if (_objetoDerecha)
        {
            //Quaternion _rot = transform.rotation;
            //transform.up = _objetoDerecha.normal;
            //float _z = transform.eulerAngles.z;
            //transform.rotation = _rot;
            
            Quaternion _angulo = Quaternion.FromToRotation(transform.up, _objetoDerecha.normal) * transform.rotation;
            transform.rotation = Quaternion.Lerp(transform.rotation,_angulo,8*Time.deltaTime);
            if(Quaternion.Angle(transform.rotation,_angulo)<1f)
            transform.rotation = _angulo;
            //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,-Vector3.forward*_z,5*Time.deltaTime);
            //transform.Rotate(Vector3.forward,_z-transform.eulerAngles.z);
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, _z);
        }

        puedeMoverse = true;
        //MoverAraña();
        puntoAnterior = _objetoAbajo.point;
    }

    private void Caer()
    {
        Quaternion _angulo = Quaternion.FromToRotation(transform.up, Vector3.up) * transform.rotation;
        transform.rotation = _angulo;
        cuerpo.gravityScale = 1;
    }

    private void Girar(bool _i,bool _a, bool _d,Vector3 _normal) {
        cuerpo.gravityScale = 0;
        cuerpo.velocity = Vector2.zero;
        // evita que la araña gire infinitamente 
        RaycastHit2D _rayoDetector = Utilidades.Rayo(transform.position,(puntoAnterior- transform.position).normalized*distanciaContacto, distanciaContacto*2,filtroSensores);
        if (!_rayoDetector)
        {
            enGiro = false;
            return;
        }
        
        //MoverAraña();
        //transform.RotateAround(puntoRotacion.position,Vector3.forward,-80*Time.deltaTime*velocidad);
        //transform.RotateAround(Vector3.Lerp(puntoAnterior,puntoRotacion.position,0.5f),Vector3.forward,-80*Time.deltaTime*velocidad);
        if (!_i && !_a && _d && Vector3.Distance(transform.up,_normal)>anguloGiro)
        {
            enGiro = false;
            return;
        }

        anguloGiro = Vector3.Distance(transform.up, _normal);
    }
    private void MoverAraña(Vector2 _direccion) {
        //Utilidades.Rayo(transform.position, Vector3.Cross(transform.up,transform.forward),1);
        
        if (_direccion.x!=0)
        {
            if (Mathf.Sign(_direccion.x) != direccionAnterior)
            {
                Quaternion _angulo = Quaternion.FromToRotation(transform.right, -transform.right) * transform.rotation;
                transform.rotation = _angulo;
                //Vector3.
                //float _a = Mathf.Sign(_direccion.x) * 90 - 90;
                //transform.eulerAngles = new Vector3();
                //transform.RotateAround(puntoRotacion.position,Vector3.up,180);
                //transform.eulerAngles = new Vector3(transform.eulerAngles.x, ejeNormal.y + _y,transform.eulerAngles.z);
                //transform.eulerAngles = _vec;
                
            }
            direccionAnterior = Mathf.Sign(_direccion.x);
        }

        if (enGiro)
        {
            transform.RotateAround(puntoRotacion.position, transform.forward, -150 * Time.deltaTime * velocidad);
            return;
        }
        


        if(!puedeMoverse) return;
        //if(_direccion.x!=0)fa
        //transform.right *= _direccion.x;

        Vector3 _mov = (transform.right * Mathf.Abs(_direccion.x)) - transform.up;
        //Vector2 _dir = Vector2.Lerp(cuerpo.velocity,_mov*velocidad,10*Time.deltaTime);
        cuerpo.velocity = _mov*velocidad;
        cuerpo.gravityScale = 0;
    }

    public override void Entrar() {
        base.Entrar();
        enTransferencia = true;
        Caer();
        cuerpo.velocity = Vector2.zero;
        StartCoroutine(Transferencia());
    }
    IEnumerator Transferencia() {
        yield return new WaitForSeconds(1);
        enTransferencia = false;
    }
    
    public override void Saltar(){
        Caer();
        return;
        if(!sensores.EnSuelo()) return;
        if(enSalto) return;
        if(enGiro) return;
        enSalto=true;
        StartCoroutine(Saltando());
    }
    IEnumerator Saltando(){
        //cuerpo.velocity = new Vector2(cuerpo.velocity.x*transform.right.x,cuerpo.velocity.y*transform.right.y);
        cuerpo.AddForce(transform.up*fuerzaSaltoActual);
        cuerpo.gravityScale = 1;
        yield return new WaitForSeconds(0.2f);
        enSalto = false;
    }

}
