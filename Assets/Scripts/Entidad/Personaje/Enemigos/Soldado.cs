﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IA.MaquinaEstado;

public class Soldado : Enemigo {
    [SerializeField]
    private float rangoAtaque;

    private Patrulla patrulla;

    private Observacion observacion;

    [SerializeField]
    private float cadencia;

    [SerializeField]
    private Transform arma;

    [SerializeField]
    private GameObject prefabBala;

    [SerializeField]
    private Transform puntoDisparo;

    protected override void Start() {
        base.Start();
        patrulla = new Patrulla(this);
        observacion = new Observacion(this);

    }

    public override void Entrar()
    {
        base.Entrar();
        Quaternion _angulo2 = Quaternion.FromToRotation(arma.up, Vector3.up) * arma.rotation;
        arma.rotation = _angulo2;
        Parar();
    }

    protected override void Update()
    {
        base.Update();
        if(enPosecion) return;
        if (observacion.Realizar(rangoAtaque) && observacion.objetivo.CompareTag("Player")) {
            Mover(Vector2.zero);
            Quaternion _angulo = Quaternion.FromToRotation(arma.up, arma.position-observacion.objetivo.GetComponent<Controlador_entidad>().Centro.position) * arma.rotation;
            arma.rotation = Quaternion.Lerp(arma.rotation,_angulo,10*Time.deltaTime);
            if (Quaternion.Angle(arma.rotation, _angulo) < 1f)
            {
                arma.rotation = _angulo;
                Disparar();
            }
            return;
        }
        Quaternion _angulo2 = Quaternion.FromToRotation(arma.up, Vector3.up) * arma.rotation;
        arma.rotation = Quaternion.Lerp(arma.rotation,_angulo2,8*Time.deltaTime);
        if (enDisparo)
            Parar();
        patrulla.Realizar();
        
    }
    private bool enDisparo;

    private void Parar() { 
        StopAllCoroutines();
        enDisparo = false;
    }
    private void Disparar() { 
        if(enDisparo) return;
        enDisparo = true;
        StartCoroutine(Disparando());
    }

    IEnumerator Disparando() {
        while (true)
        {   
            if(prefabBala && puntoDisparo)
            Instantiate(prefabBala,puntoDisparo).transform.parent = null;

            if(cadencia>=0)
                yield return new WaitForSeconds(cadencia);
            else
                yield return new WaitForSeconds(0);
        }
    }

    public override void Mover(Vector2 _direccion)
    {
        MoverSoldado(_direccion);
    }

    private void MoverSoldado(Vector2 _direccion) { 
        base.Mover(_direccion);
        if(_direccion.x!=0)
        transform.right = Vector2.right * _direccion.x;
    }
    public override void Destruirse() {
       
            Destroy(gameObject);
    }

}
