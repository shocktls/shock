﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : Controlador_personaje {

    [SerializeField]
    protected Transform patrullaInicio;
    [SerializeField]
    protected Transform patrullaFin;

    public void ConfigurarPatrulla(Transform _inicio, Transform _fin) { 
        if(_inicio)
            patrullaInicio = _inicio;
        if(_fin)
            patrullaFin = _fin;
    }

    public Vector3[] PuntosPatrulla() {

        if(patrullaInicio)
            if(patrullaFin)
                return new Vector3[] {patrullaInicio.position,patrullaFin.position};
            else
                return new Vector3[] {patrullaInicio.position};

        return null;
    }

    [SerializeField]
    protected Transform puntoMira;
    
    public Transform PuntoMira { get { return puntoMira; } }


}
