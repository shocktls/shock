﻿using UnityEngine;

namespace IA.MaquinaEstado
{
    public class Patrulla
    {
        private bool puntoActual;
        
        private Vector3 personajePosicion{
            get{
                return personaje.transform.position;
            }
        }

        private Vector3[] puntosPatrulla { 
            get {
                return personaje.PuntosPatrulla();
            } 
        }

        public float direccion = 1;

        private Enemigo personaje;

        public Patrulla(Enemigo _personaje) {
            personaje = _personaje;
        }

        public void Realizar() 
		{   
            if(personaje.PuntosPatrulla()==null) return;

            float _distancia=Mathf.Infinity;

            _distancia = Vector3.Distance(puntosPatrulla[puntoActual? 1:0],personajePosicion);
            
            bool _cerca = _distancia < 0.3f;

            if (_cerca)
                Cambiar();
            //personaje.transform.right = Vector2.right * direccion;

            personaje.Mover(Vector2.right * direccion);
        }

        public void Cambiar() { 
            puntoActual = !puntoActual;
            direccion = -direccion;
        }
    }
}
