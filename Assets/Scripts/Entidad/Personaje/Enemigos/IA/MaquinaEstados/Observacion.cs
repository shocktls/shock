﻿using UnityEngine;

namespace IA.MaquinaEstado
{
    public class Observacion
    {
        private Vector2 puntoMira { get { return personaje.PuntoMira.position; } }
        private Enemigo personaje;

        public Transform objetivo { get; private set; }

        private bool alerta;
        private ContactFilter2D filtro;

        public Observacion(Enemigo _personaje)
        {
            personaje = _personaje;
        }
        RaycastHit2D _objetivo;
        public bool Realizar(float _distancia)
        {
            if (!alerta)
            {
                objetivo = null;
                //_objetivo = Physics2D.Raycast(puntoMira, personaje.transform.right,_distancia);
                _objetivo = Utilidades.Rayo(puntoMira, personaje.transform.right, _distancia,filtro);
                objetivo = _objetivo.transform;
                if (!_objetivo) return false;
            }
            else     
                //_objetivo = Physics2D.Raycast(puntoMira, objetivo.position - personaje.transform.position,_distancia);
                _objetivo = Utilidades.Rayo(puntoMira, objetivo.position - personaje.transform.position,_distancia,filtro);

            if (!_objetivo) return false;
            
            objetivo = _objetivo.transform;

            if(objetivo.CompareTag("Player"))
                alerta = true;
            else 
                alerta = false;

            return true;
        }

    }
}
