﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Shock : Controlador_personaje
{
    [SerializeField]
    private float fuerzaImpulso;
    public bool enImpulso;
    private bool enAire;
    private bool puedeMoverse=true;
    private bool unaVez;
    public bool impulso;
    [HideInInspector]
    public bool impulsoBloq;
    public override void Mover(Vector2 _direccion)
    {
        if (enImpulso) return;
        if(!puedeMoverse) return;
        if(_direccion.x !=0)
        transform.right = Vector2.right * _direccion.x;
        cuerpo.velocity = new Vector2(_direccion.x * velocidad, cuerpo.velocity.y);
    }
    public override void Saltar()
    {   
        if (enImpulso) return;
        if (!puedeMoverse) return;
        if(!sensores.EnSuelo()) return;
        base.Saltar();
        anim.Play("Salto_a");
        anim.SetBool("Salto",true);
    }

    public override void Salir()
    {
        base.Salir();
        gameObject.SetActive(false);
    }

    public override void Entrar()
    {
        base.Entrar();
        gameObject.SetActive(true);
    }

    protected override void Update()
    {
        base.Update();
        if(vidaActual<=0)
            SceneManager.LoadScene(1);
        if(sensores.EnSuelo())
            anim.SetBool("Salto",false);

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            StopCoroutine(CargarImpulso());
            StartCoroutine(CargarImpulso());
            if (!impulsoTiempo)
            {   
                impulsoTiempo = true;
                return;
            }
            if(Input.GetKeyDown(KeyCode.A))
                direccionImpulso = Vector2.left;
            if(Input.GetKeyDown(KeyCode.W))
                direccionImpulso = Vector2.up;
            if(Input.GetKeyDown(KeyCode.S))
                direccionImpulso = Vector2.down;
            if(Input.GetKeyDown(KeyCode.D))
                direccionImpulso = Vector2.right;
            
            AccionPrimera();
        }
    }
    private bool impulsoTiempo;
    private Vector2 direccionImpulso;
    public override void AccionPrimera()//impulso(dash)
    {
        // StopCoroutine(CargarImpulso());
        // StartCoroutine(CargarImpulso());

        // if (!impulsoTiempo) { impulso = true; return; }
        if(impulsoBloq) return;
        if(!impulso) return;
        if (enImpulso) return;
        if(enAire) return;
        enAire = true;
        enImpulso = true;
        cuerpo.gravityScale = 0;
        StartCoroutine(Impulso());
    }
    IEnumerator CargarImpulso() {
        yield return new WaitForSeconds(.2f);
        impulsoTiempo = false;
    }
    public override void AccionSegunda(Vector2 _direccion,bool _estado)//magnetizar
    {   
        if(enImpulso) return;
        puedeMoverse = !_estado;
        if (!_estado) { Desmagnetizar(); return; }
        if (!unaVez)
        {
            unaVez = true;
            cuerpo.velocity = Vector2.zero;
        }

        RaycastHit2D _objetivo = Physics2D.Raycast(transform.position,_direccion,100);
        if(_objetivo)
        if(_objetivo.transform.CompareTag("Metalico"))
        Magnetizar(_direccion);
    }

    private void Magnetizar(Vector2 _direccion) {
        cuerpo.gravityScale = 0;
        cuerpo.velocity = _direccion*5;
        transform.up = -_direccion;
    }
    private void Desmagnetizar() {
        unaVez = false;
        //transform.up = Vector3.up;
        cuerpo.gravityScale = 1;
    }
    private IEnumerator Impulso() {
        anim.SetBool("Dash",true);

        if(direccionImpulso == Vector2.right || direccionImpulso==Vector2.left)
            anim.Play("DashLateral");
        if(direccionImpulso == Vector2.up)
            anim.Play("DashArriba");
        if(direccionImpulso==Vector2.down)
            anim.Play("DashAbajo");

        Vector2 _temp = new Vector2(Mathf.Abs(direccionImpulso.y), Mathf.Abs(direccionImpulso.x));
        cuerpo.velocity =  Utilidades.MultiVector2(cuerpo.velocity,_temp);
        cuerpo.AddForce(fuerzaImpulso * direccionImpulso);
        yield return new WaitForSeconds(200/fuerzaImpulso);
        anim.SetBool("Dash",false);
        enImpulso = false;
        cuerpo.gravityScale = 1;
        _temp = new Vector2(Mathf.Abs(direccionImpulso.y), Mathf.Abs(direccionImpulso.x));
        cuerpo.velocity =  Utilidades.MultiVector2(cuerpo.velocity,_temp);
        while (true)
        {
            if (sensores.EnSuelo())
            { 
                enAire = false;
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
