﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Controlador_camara : MonoBehaviour {
    [SerializeField]
    private Controlador_nivel nivel;

    public SpriteRenderer fondo;

    public CinemachineVirtualCamera camaraVirtual;

    public static Controlador_camara principal;

    public Controlador_capas capas;

    private GameObject tipo;

    private void Start() {
        principal = this;
        camaraVirtual.Follow = nivel.controlInteraccion.transform;
        tipo = capas.capaActual;
    }
    private void Update()
    {
        if (tipo != capas.capaActual)
        {
            camaraVirtual.GetComponent<CinemachineConfiner>().InvalidatePathCache();
            camaraVirtual.GetComponent<CinemachineConfiner>().m_BoundingShape2D = capas.capaActual.transform.GetChild(0).GetComponent<Collider2D>();
            tipo = capas.capaActual;
        }
    }

}
