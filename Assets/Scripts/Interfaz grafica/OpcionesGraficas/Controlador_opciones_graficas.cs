using UnityEngine;
using System.IO;

public class Controlador_opciones_graficas : MonoBehaviour
{

    private Administrador_opciones_graficas opc = new Administrador_opciones_graficas();

    void Start()
    {
        CargarOpciones();

    }

    public void AplicarPantalla(int _nuevoModoPantalla)
    {
        if (opc.modoPantalla != _nuevoModoPantalla)
            opc.modoPantalla = _nuevoModoPantalla;

        // switch (opc.modoPantalla)                                // descomentar cuando se vaya a implementar
        // {
        //     case 0:
        //         Screen.fullScreen = true;
        //         break;
        //     case 1:
        //         Screen.fullScreen = false;
        //         break;
        // }
    }

    public void AplicarBrillo(float _nuevoBrillo)
    {
        if (_nuevoBrillo != opc.valorBrillo)
            opc.valorBrillo = _nuevoBrillo;
    }

    public void AplicarVolumenEfectos(float _nuevoVolumenSFX)
    {
        if (_nuevoVolumenSFX != opc.valorVolumenEfectos)
            opc.valorVolumenEfectos = _nuevoVolumenSFX;
    }

    public void AplicarVolumenMusica(float _nuevoVolumenMusica)
    {
        if (_nuevoVolumenMusica != opc.valorVolumenMusica)
            opc.valorVolumenMusica = _nuevoVolumenMusica;
    }

    public void AplicarIdioma(int _nuevoIdioma)
    {
        if (_nuevoIdioma != opc.idioma)
            opc.idioma = _nuevoIdioma;
    }

    public void GuardarOpciones()
    {
        string temp;
        temp = JsonUtility.ToJson(opc);
        File.WriteAllText(Application.dataPath + "/Opciones.json", temp);
        Debug.Log("Se guardaron los cambios en la PC");
    }

    private void CargarOpciones()
    {
        if (File.Exists(Application.dataPath + "/Opciones.json"))
        {
            string temp;
            temp = File.ReadAllText(Application.dataPath + "/Opciones.json");
            Administrador_opciones_graficas opcGuardadas = JsonUtility.FromJson<Administrador_opciones_graficas>(temp);
            AplicarPantalla(opcGuardadas.modoPantalla);
            AplicarBrillo(opcGuardadas.valorBrillo);
            AplicarVolumenEfectos(opcGuardadas.valorVolumenEfectos);
            AplicarVolumenMusica(opcGuardadas.valorVolumenMusica);
            AplicarIdioma(opcGuardadas.idioma);
            Debug.Log("Se cargó la configuración previa");
        }
        else
            Debug.Log("No se hallaron cambios previos, se cargó la configuración por defecto");
    }

    public int GuardPantalla()
    {
        return opc.modoPantalla;
    }
    public float GuardBrillo()
    {
        return opc.valorBrillo;
    }

    public float GuardVolumenEfectos()
    {
        return opc.valorVolumenEfectos;
    }

    public float GuardVolumenMusica()
    {
        return opc.valorVolumenMusica;
    }

    public int GuardIdioma()
    {
        return opc.idioma;
    }


}