﻿using UnityEngine.UI;
using UnityEngine;
[System.Serializable]

public class Referencias_opciones_graficas : MonoBehaviour
{
    private Controlador_opciones_graficas opciones_Graficas;

    [SerializeField]
    private Dropdown modoPantalla;
    [SerializeField]
    private Slider brillo;
    [SerializeField]
    private Slider volumenEfectos;
    [SerializeField]
    private Slider volumenMusica;
    [SerializeField]
    private Dropdown idioma;

    private void Start()
    {
        opciones_Graficas = transform.parent.GetComponent<Controlador_opciones_graficas>();

        modoPantalla.value = opciones_Graficas.GuardPantalla();
        brillo.value = opciones_Graficas.GuardBrillo();
        volumenEfectos.value = opciones_Graficas.GuardVolumenEfectos();
        volumenMusica.value = opciones_Graficas.GuardVolumenMusica();
        idioma.value = opciones_Graficas.GuardIdioma();
    }

    public void AplicarCambios()
    {
        opciones_Graficas.AplicarPantalla(modoPantalla.value);
        opciones_Graficas.AplicarBrillo(brillo.value);
        opciones_Graficas.AplicarVolumenEfectos(volumenEfectos.value);
        opciones_Graficas.AplicarVolumenMusica(volumenMusica.value);
        opciones_Graficas.AplicarIdioma(idioma.value);
        opciones_Graficas.GuardarOpciones();
    }
}
