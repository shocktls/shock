﻿using UnityEngine;
[System.Serializable]

public struct Administrador_opciones_graficas
{

    public int modoPantalla;
    public float valorBrillo;
    public float valorVolumenEfectos;
    public float valorVolumenMusica;
    public int idioma;

}
