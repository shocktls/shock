﻿using UnityEngine;

public class Administrador_interfaz_grafica : MonoBehaviour {

    [SerializeField]
    protected GameObject prefabHud;
    [SerializeField]
    protected GameObject prefabPaneles;

    public Controlador_paneles paneles{ get;protected set; }
    public Controlador_hud hud{ get;protected set; }
    
    protected Controlador_juego juego;
}
