﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_hud : Administrador_hud {

    public void Incializar(Controlador_nivel _juego) {
        juego = _juego;
        barras.Iniciar(ValoresBarras(true));
        this.enabled = true;
    }

    private void Update() {
		
        barras.Actualizar(ValoresBarras(false));
    }

    public void QuitarExtra() {
        barras.EliminarBarraPersonajeExtra();
    }
    public void CargarExtra() {
        barras.CargarBarraPersonajeExtra(ValoresBarras(true));
    }

    private int[] ValoresBarras(bool _inicial) {
        int[] i = new int[3];
        i[0] = _inicial? juego.PersonajeShock.vidaMaxima:juego.PersonajeShock.vidaActual;
		i[1] = _inicial? juego.PersonajeShock.energiaMaxima:juego.PersonajeShock.energiaActual;
		if(juego.adminPersonajeExtra)
            i[2] = _inicial? juego.adminPersonajeExtra.vidaMaxima:juego.adminPersonajeExtra.vidaActual;
        else
			i[2] = 0;
        return i;
    }
}
