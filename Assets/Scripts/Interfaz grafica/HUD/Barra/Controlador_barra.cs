﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_barra : Administrador_barra {

	private Proyector_barra proyectorBarras = new Proyector_barra();

	private int vidaPersonajeShock;
	private int vidaPersonajeExtra;
	private int energiaPersonajeShock;

    public void Iniciar(int[] _valores) {
        CargarValores(_valores);
    }

    private void CargarValores(int[] valor) {
        int _vidaMaxima = Mathf.CeilToInt(valor[0]/100f);//calcula cuantas barras de vida tiene shock
		int _energiaMaxima = Mathf.CeilToInt(valor[1]/100f);//calcula cuantas barras de energia tiene shock
		CargarBarraVidaSchok(proyectorBarras.GenerarBarras(_vidaMaxima,prefabBarraVida,lugarVidaShock));//instancia las barras de vida de shock
		CargarBarraEnergiaShock(proyectorBarras.GenerarBarras(_energiaMaxima,prefabBarraEnergia,lugarEnergiaShock));//instancia las barras de energia de shock

		if (valor[2]!=0)//verifica si hay un personaje extra
            CargarBarraPersonajeExtra(valor);
	}

    public void Actualizar(int[] valor){
		EstadoPersonaje(valor);
	}

    public void CargarBarraPersonajeExtra(int[] valor) {
		int vidaMaxima = Mathf.CeilToInt(valor[2]/100f); //calcula cuantas barras de vida tiene el personaje extra
        CargarBarraVidaExtra(proyectorBarras.GenerarBarras(vidaMaxima,prefabBarraVidaExtra,lugarVidaExtra));//instancia las barras de vida del personaje extra
    }
    public void EliminarBarraPersonajeExtra() {
        proyectorBarras.LimpiarBarras(lugarVidaExtra,ref barrasVidaExtra);
    }

    private void EstadoPersonaje(int[] valor){
		if(valor[0]!=vidaPersonajeShock)//verfica si hay algun cambio en la vida de shock
			proyectorBarras.ActualizarBarras(barrasVidaShock,valor[0],valorBarraVida);
		
		if (valor[1]!=energiaPersonajeShock)//verifica si hay algun canbio en la energia de shock;
			proyectorBarras.ActualizarBarras(barrasEnergiaShock,valor[1],valorBarraEnergia);
		
		//actualiza los datos temporales
        vidaPersonajeShock = valor[0];
        energiaPersonajeShock = valor[1];

        if (valor[2]!=0){//verifica si hay un personaje extra
            if (valor[2] != vidaPersonajeExtra) //verifica si hay cambios en la vida actual del personaje extra
                proyectorBarras.ActualizarBarras(barrasVidaExtra, valor[2],valorBarraVidaExtra);
            vidaPersonajeExtra = valor[2];//actualiza los datos temporales
        }
        
    }
	
	
	

}
