﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Proyector_barra{

    public List<GameObject> GenerarBarras(int _cantidad,GameObject _barra,Transform _lugarBarra) {
        List<GameObject> barras = new List<GameObject>();
        while(barras.Count<_cantidad)
            barras.Add(MonoBehaviour.Instantiate(_barra,_lugarBarra));
        return barras;
    }
    
    public void LimpiarBarras(Transform _contenedorBarras,ref List<GameObject> _barras) { 
        _barras.Clear();
        foreach(Transform i in _contenedorBarras)
            MonoBehaviour.Destroy(i.gameObject);
    }

    public void ActualizarBarras(List<GameObject> _barras,int _vida,int valorBarra){
        int _barraVida = _vida<0? 0:_vida / valorBarra;
        for (int i = 0; i < _barras.Count;i++)
        {	
			if(i<_barraVida)
			_barras[i].GetComponent<Slider>().value = valorBarra;
			else
                _barras[i].GetComponent<Slider>().value = 0;

        }
		if(_barraVida<_barras.Count)
		_barras[_barraVida].GetComponent<Slider>().value = (_vida%valorBarra)/(float)valorBarra; 
    }
}
