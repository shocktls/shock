﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Administrador_barra : MonoBehaviour {
    //PD. estas 9 variable seran privadas en el futuro
    public GameObject prefabBarraVida;//prefab  de la barra de vida de shock
    public GameObject prefabBarraVidaExtra;//prefab  de la barra de vida extra
    public GameObject prefabBarraEnergia;//prefab  de la barra de energia de Shock
    public Transform lugarVidaShock;// lugar donde instaciar las barras de vida de shock
    public Transform lugarVidaExtra;// lugar donde instaciar las barras de vida del personaje extra
    public Transform lugarEnergiaShock;// lugar donde instaciar las barras de energia de shock

    public List<GameObject> barrasVidaShock = new List<GameObject>();
	public List<GameObject> barrasEnergiaShock= new List<GameObject>();
	public List<GameObject> barrasVidaExtra = new List<GameObject>();

    public int valorBarraVida = 100;//valor en puntos de cada barra de vida
	public int valorBarraVidaExtra = 100;//valor en puntos de cada barra de vida extra
	public int valorBarraEnergia = 100;//valor en puntos de cada barra de energia

    protected void CargarValoresBarras(int _barraVida, int _barraVidaExtra, int _barraEnergia)
    {
        valorBarraVida = _barraVida;
        valorBarraVidaExtra = _barraVidaExtra;
        valorBarraEnergia = _barraEnergia;
    }


    //...codigo para limpiar luego
    protected void CargarBarraVidaSchok(List<GameObject> _barras){
        barrasVidaShock = _barras;
        Debug.Log("Se han generado las barras de vida de shock");
    }

    protected void CargarBarraVidaExtra(List<GameObject> _barras){
        barrasVidaExtra = _barras;
        Debug.Log("Se han generado las barras de vida del personaje extra");
    }

    protected void CargarBarraEnergiaShock(List<GameObject> _barras) {
        barrasEnergiaShock= _barras;
        Debug.Log("Se han generado las barras de energia de shock");
    }
    
}
