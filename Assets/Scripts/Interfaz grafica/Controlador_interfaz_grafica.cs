﻿using UnityEngine;

public class Controlador_interfaz_grafica : Administrador_interfaz_grafica
{

    private void Awake()
    {
        juego = GetComponentInParent<Controlador_juego>();
    }

    public void Start()
    {
        foreach (Transform i in transform)
            Destroy(i.gameObject);
        paneles = Instantiate(prefabPaneles, transform).GetComponent<Controlador_paneles>();
    }

    public void Iniciarlizar(Controlador_nivel _juego)
    {

    }
    public void CargoNivel()
    {
        paneles.Apagar();
    }
    public void CargarHud()
    {
        if (!hud)
            hud = Instantiate(prefabHud, transform).GetComponent<Controlador_hud>();
    }

    public void EnviarJuego(string _nivel)
    {
        juego.RecibirNivel(_nivel);
    }

}
