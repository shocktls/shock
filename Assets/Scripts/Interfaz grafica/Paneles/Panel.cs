﻿using UnityEngine;

public class Panel : MonoBehaviour
{

    private Controlador_paneles panel;

    private void Start()
    {
        panel = transform.parent.GetComponent<Controlador_paneles>();
    }

    public void Jugar(string _nivel)
    {
        panel.Enviar(_nivel);
    }

    public void CargarPanel(GameObject _panel)
    {
        panel.InstanciarPanel (_panel);
    }


}
