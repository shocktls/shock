﻿using UnityEngine;

public class Controlador_paneles : Administrador_paneles
{

    public void Awake()
    {
        interfazGrafica = GetComponentInParent<Controlador_interfaz_grafica>();
    }

    public void Start()
    {
        PanelActual = Instantiate(prefabPanelPrincipal, transform);
    }

    public void Enviar(string _nivel)
    {
        interfazGrafica.EnviarJuego(_nivel);
    }

    public void Apagar()
    {
        gameObject.SetActive(false);
    }

    public void InstanciarPanel(GameObject _panelRecibido)
    {
        Destroy (PanelActual);
        PanelActual = Instantiate(_panelRecibido, transform);
    }

}
