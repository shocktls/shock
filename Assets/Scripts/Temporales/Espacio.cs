﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espacio : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D _obj) { 
		if(!_obj.GetComponent<Araña>()) return;
        GetComponent<Animator>().Play("Espacio");
    }
	void OnTriggerExit2D(Collider2D _obj) { 
		if(!_obj.GetComponent<Araña>()) return;
        GetComponent<Animator>().Play("Vacio");
    }
}
