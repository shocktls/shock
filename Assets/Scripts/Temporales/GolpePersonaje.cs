﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpePersonaje : MonoBehaviour {

	private bool enGolpe;
    float temp;
    void OnTriggerStay2D(Collider2D _obj)
    {	
		if(enGolpe) return;
        if(!_obj.GetComponent<Enemigo>()) return;
        enGolpe = true;
        transform.parent.GetComponent<Shock>().enImpulso = true;
        temp = Mathf.Sign(transform.parent.position.x - _obj.transform.position.x);
        
        transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        transform.parent.GetComponent<Rigidbody2D>().AddForce(Vector2.right*temp*450);
        StartCoroutine(Golpe());
    }

    IEnumerator Golpe()
    {
		transform.parent.GetComponent<Animator>().Play("Golpe");
        transform.parent.GetComponent<Animator>().SetBool("Golpe",true);
		yield return new WaitForSeconds(.2f);
        transform.parent.GetComponent<Rigidbody2D>().AddForce(-Vector2.right * temp * 300);
		yield return new WaitForSeconds(.2f);
		transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        yield return new WaitForSeconds(.4f);
		transform.parent.GetComponent<Animator>().SetBool("Golpe",false);
		transform.parent.GetComponent<Shock>().enImpulso = false;
        StartCoroutine(Carga());
    }
    IEnumerator Carga() {
        
        bool flip =false;
        for (int i = 0; i < 10; i++)
        {
            transform.parent.GetComponent<SpriteRenderer>().color = flip ? Color.white : Color.red;
            flip = !flip;
			yield return new WaitForSeconds(0.1f);
        }
        enGolpe = false;
    }


}
