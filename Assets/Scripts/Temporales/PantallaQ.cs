﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaQ : MonoBehaviour {

    private Animator anim;
    private bool final;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public virtual void OnTriggerEnter2D (Collider2D _obj) {
        if(final) return;
        if (_obj.GetComponent<Controlador_entidad>() && _obj.GetComponent<Controlador_entidad>().enPosecion)
            anim.Play("Activo");

    }


    public virtual void OnTriggerExit2D (Collider2D _obj) {
        if(final) return;
        if (_obj.GetComponent<Controlador_entidad>() && _obj.GetComponent<Controlador_entidad>().enPosecion)
            
            anim.Play("Idle");
        
    } 
}
