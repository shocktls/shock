﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamaraSeguir : MonoBehaviour {

    public Transform[] objetivos;

    private int contador;
    private CinemachineVirtualCamera camara;

    void Awake() {
        camara = GetComponent<CinemachineVirtualCamera>();
    }
    void Start() {
        Cambiar();
    }

    void Update () {
		if(Input.GetKeyDown(KeyCode.H))
            Cambiar();
    }
    void Cambiar() { 
		if(objetivos.Length==0) return;
        camara.Follow = objetivos[contador];
        contador++;
		if(contador==objetivos.Length)
            contador = 0;
		
    }

}
