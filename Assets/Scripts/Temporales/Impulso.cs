﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Impulso : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D _obj) {
        if (_obj.GetComponent<Shock>())
        {
            _obj.GetComponent<Shock>().impulso = true;
            Destroy(gameObject);
        }
    }
}
