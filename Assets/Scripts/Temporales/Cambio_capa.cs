﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Cambio_capa : MonoBehaviour {
    
    public GameObject capaA;
	public GameObject capaB;
    public SpriteRenderer puertas;

    private GameObject capa { get { return transform.root.GetComponent<Controlador_capas>().capaActual;} set{transform.root.GetComponent<Controlador_capas>().CambiarCapa(value);}}
    private bool enCambio=false;

    void OnTriggerStay2D(Collider2D _obj) {
        if(!capaA || !capaB) return;
        if (_obj.GetComponent<Controlador_entidad>() && !enCambio && _obj.GetComponent<Controlador_entidad>().enPosecion)
            if(Input.GetKey(KeyCode.E) && !enCambio)
			{
                Controlador_nivel.Interactor().Estado(false);
                if(Controlador_nivel.Jugador().GetComponent<Shock>())
                Controlador_nivel.Jugador().GetComponent<Shock>().impulsoBloq = true;
                enCambio = true;
                GetComponent<Animator>().Play("AnimPuertaAbrir");
                Entradas.principal.Interaccion(false);
                StartCoroutine(Entrar());

            }
    }
    IEnumerator Entrar() {
        for (int i = 0; i <= 50; i++)
        {
            Controlador_camara.principal.fondo.color = new Color(0, 0, 0, i/50f);
            yield return new WaitForFixedUpdate();
        }
        
        capa = (capa == capaA)? capaB:capaA;
        if(!Controlador_nivel.Jugador().GetComponent<Shock>()) //Error al salir de un enemigo mientras cruzas la puerta
        Controlador_nivel.Jugador().transform.parent = capa.transform;
        //StartCoroutine(Salir());
    }
	IEnumerator Salir() {
		
        for (int i = 30; i >= 0; i--)
        {
            Controlador_camara.principal.fondo.color = new Color(0, 0, 0, i/30f);
            yield return new WaitForFixedUpdate();
        }
    }

    public void Cambio(int _i){
        puertas.sortingOrder = _i;
    }
    public void Finalizar()
    {   
        Controlador_nivel.Interactor().Estado(true);
        if(Controlador_nivel.Jugador().GetComponent<Shock>())
            Controlador_nivel.Jugador().GetComponent<Shock>().impulsoBloq = false;
        enCambio = false;
        Entradas.principal.Interaccion(true);
        GetComponent<Animator>().Play("Vacio");
    }
}
