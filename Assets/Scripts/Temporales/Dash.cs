﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {

    private bool puedeActivar;
    private Shock sujeto;
    private bool final;
    void Update()
    { 	
		if(final) return;
        if(!puedeActivar) return;
        if (Input.GetKeyDown(KeyCode.E))
        {
            sujeto.impulso = true;
            final=true;
            GetComponent<Animator>().Play("Activacion");
        }
    }

    void OnTriggerEnter2D(Collider2D _obj)
    { 	
		if(final) return;
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        sujeto = _obj.GetComponent<Shock>();
        puedeActivar = true;
		GetComponent<Animator>().Play("Prendido");
    }
	void OnTriggerExit2D(Collider2D _obj)
    { 	
		if(final) return;
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        sujeto = _obj.GetComponent<Shock>();
        puedeActivar = false;
		GetComponent<Animator>().Play("Apagado");
    }
}
