﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaFlecha : MonoBehaviour {

    private Animator anim;
    public GameObject pantalla;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    public virtual void OnTriggerEnter2D (Collider2D _obj) {
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        if(!_obj.GetComponent<Controlador_entidad>().enPosecion) return;
        Destroy(pantalla);
        GetComponent<SpriteRenderer>().enabled = true;
        Destroy(this);
    }
}
