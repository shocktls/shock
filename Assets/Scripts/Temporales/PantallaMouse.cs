﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaMouse : Pantalla {

    public bool paso;
    public Controlador_nivel nivel;
    IEnumerator Start() {
        yield return new WaitForEndOfFrame();
        nivel.controlInteraccion.Estado(false);
    }

    public override void OnTriggerEnter2D (Collider2D _obj) {
        if(paso) return;
        if(!_obj.GetComponent<Controlador_entidad>()) return;
        base.OnTriggerEnter2D(_obj);
        nivel.controlInteraccion.Estado(true);
    }   
    public void OnTriggerStay2D(Collider2D _obj)
    { 
         if (_obj == Controlador_nivel.Jugador().GetComponent<Collider2D>())
            if (Input.GetMouseButton(1))
            {
                paso = true;
                GetComponent<Animator>().Play("ActivoSegundo");
            }

    }

    public override void OnTriggerExit2D (Collider2D _obj) {
        //if(paso) return;
        base.OnTriggerExit2D(_obj);
        
    } 
}
