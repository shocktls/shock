﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pantalla : MonoBehaviour {

    private Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public virtual void OnTriggerEnter2D (Collider2D _obj) {
        
        if (_obj.GetComponent<Controlador_entidad>() && _obj.GetComponent<Controlador_entidad>().enPosecion)
            anim.Play("Activo");

    }   
    public virtual void OnTriggerExit2D (Collider2D _obj) {
        
        if (_obj.GetComponent<Controlador_entidad>() && _obj.GetComponent<Controlador_entidad>().enPosecion)
            
            anim.Play("Idle");
        
    } 
}
