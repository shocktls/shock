﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

    [SerializeField]
    private float velocidad;
    [SerializeField]
    private int daño;
    private void Start () {
        GetComponent<Rigidbody2D>().AddForce(velocidad*transform.up);
        Destroy(gameObject, 3);
    }

    private void OnTriggerEnter2D(Collider2D _obj) {
        if (_obj.CompareTag("Player"))
        {
            _obj.GetComponent<Controlador_personaje>().RecibirDaño(daño);
            Destroy(gameObject);
        }
        if (_obj.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }
}
