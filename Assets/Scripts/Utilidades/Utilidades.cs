﻿using UnityEngine;

public struct Utilidades {

    static float pixel = 1f / 16f;

    public static Vector2 DireccionMouse(Vector3 _origen){ 
		Vector2 _direccion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
       	return  (_direccion - new Vector2(_origen.x,_origen.y)).normalized;
    }
    public static Vector2 DireccionMouseRectangular(Vector3 _origen){ 
		Vector2 _direccion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _direccion -= new Vector2(_origen.x, _origen.y);
        if(Mathf.Abs(_direccion.y)>Mathf.Abs(_direccion.x))
            _direccion = Vector2.up * Mathf.Sign(_direccion.y);
        else
            _direccion = Vector2.right * Mathf.Sign(_direccion.x);

        //_direccion = new Vector2(Mathf.Round(_direccion.x),Mathf.Round(_direccion.y));
        return  _direccion;
    }
    public static Vector2 ConvetidorVector(Vector3 _vector3) {
        Vector2 _vector = new Vector2(_vector3.x,_vector3.y);
        return _vector;
    }
    public static Vector2 SumaVectores(Vector3 _vector3, Vector2 _vector2)
    {
        Vector2 _vector = ConvetidorVector(_vector3) + _vector2;
        return _vector;
    }

    public static RaycastHit2D Rayo(Vector3 _origen,Vector3 _direccion,float _distacia,ContactFilter2D _filtro) {

        RaycastHit2D[] _cosas = new RaycastHit2D[1];
        int _i = Physics2D.Raycast(_origen, _direccion, _filtro, _cosas,_distacia);

        Color _c = Color.white;
        if(!_cosas[0])
            _c = Color.red;
        Debug.DrawRay(_origen,_direccion,_c);

        return _cosas[0];
    }

    public static Vector3[] Vectores(Vector2[] _vectores) {
        Vector3[] _v = new Vector3[_vectores.Length];
        int _i= 0;
        foreach (Vector3 i in _vectores)
        {
            _v[_i] = i;
            _i++;
        }
        return _v;
    }

    public static Vector3 MovimientoPixel(Vector3 _i) {
        float _x = _i.x / pixel;
        _x = Mathf.Round(_x);
        float _y = _i.y / pixel;
        _y = Mathf.Round(_y);
        return new Vector3(_x * pixel, _y * pixel, _i.z);
    }

    public static void AjustaArriba(Transform _entidad,Vector3 _arriba) { 
        Vector3 _r = _entidad.transform.eulerAngles;
        _entidad.up = _arriba;
        _entidad.eulerAngles = new Vector3(_r.x,_r.y,_entidad.eulerAngles.z);  
    }
    public static void AjustaDerecha(Transform _entidad,Vector3 _derecha) {
        int _i = (int)Mathf.Sign(_derecha.x);
        _entidad.eulerAngles += Vector3.up * (_i == 1 ? 0:180);
    }

    public static Vector3 MultiVector(Vector3 _a,Vector3 _b) {
        Vector3 _i = new Vector3(_a.x*_b.x,_a.y*_b.y,_a.z*_b.z);
        return _i;

    }
    public static Vector2 MultiVector2(Vector2 _a,Vector2 _b) {
        Vector3 _i = new Vector3(_a.x*_b.x,_a.y*_b.y);
        return _i;

    }

    public static Vector2 VectorPerpendicular(Vector2 _vector) {
        Vector2 _i = new Vector2(_vector.y,-_vector.x);
        return _i;
    }
    public static Vector3 VectorPerpendicular(Vector3 _vector) {
        Vector3 _i = new Vector3(_vector.y,-_vector.x,_vector.z);
        return _i;
    }

    public int RestarActualizar(int _resta,ref int _numero) {
		if(_numero == 0) return 0;

        if ((_numero - _resta) >= 0){
            _numero -= _resta;
            return _resta;
        }else
        {
            int temp = _numero;
            _numero = 0;
            return temp;
        }
    }
}
