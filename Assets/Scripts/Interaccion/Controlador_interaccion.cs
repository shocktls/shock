﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_interaccion : MonoBehaviour {

    private Controlador_nivel juego;
    private bool enDisparo;
    private Proyector_interaccion rayo;
    [SerializeField]
    private Transform puntoDisparo;//punto de disparo del rayo
    [SerializeField]
    private int frecuenciaRayo = 1;
    [SerializeField]
    private float amplitudRayo;
    [SerializeField]
    private int rebotes;
    [SerializeField]
    private SpriteRenderer mira;

    public bool visible { get;private set; }

    private void Awake() {
        rayo = new Proyector_interaccion(puntoDisparo,frecuenciaRayo,amplitudRayo,rebotes);
    }

    public void Iniciar(Controlador_nivel _control) {
        visible = true;
        juego = _control;
    }

    private void Update() { 
        transform.position = juego.PersonajeActual.Centro.position;
    }
    public void Estado(bool _estado) { // prendido(true) o apagado(false)
        visible = _estado;
        mira.enabled = _estado;
        rayo.PararRayo();
    }

    public void Apuntar() {
        if(enDisparo) return;
        //_posicion = Camera.main.ScreenToWorldPoint(_posicion);
        transform.up = Utilidades.DireccionMouse(transform.position); //_posicion - new Vector2(transform.position.x,transform.position.y);
    }
    public void VerificarEntidad() {
        rayo.DispararRayo();
    }
    public void CargarRayo() {
        if (rayo.Transferencia())
        {
            rayo.CancelarTransferencia();
            juego.CambiarPersonaje(rayo.objetivo.gameObject);
        }
    }
    public void CancelarCarga() {
        rayo.CancelarTransferencia();
    }
    public void DejarVerificarEndidad() { 
        rayo.PararRayo();
    }

    public void CancelarPosesion() {
        Collider2D[] _obj = Physics2D.OverlapCircleAll(juego.PersonajeActual.PuntoSalida,0.01f,1<<2);
        foreach(Collider2D _i in _obj)
            if(_i.CompareTag("Zona/Restringida"))
                return;
        if(!juego.PersonajeActual.puedeSalir) return;
        juego.SalirPersonaje();
    }

}
