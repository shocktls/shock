﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JuegoShock.Interaccion;

public class Proyector_interaccion {

    public Collider2D objetivo;
    private LineRenderer rayo;
    private Transform origen;
    private int frecuencia=1;
    private float amplitud=0.5f;
    private float aumentoCarga;//valor temporal para calcular la cargaActual
    private float cargaActual;//carga actual de transferencia
    private bool esInteractuable;
    private int rebotes;

    public Proyector_interaccion(Transform _rayo,int _frecuencia,float _amplitud,int _rebotes) {
        rayo = _rayo.GetComponent<LineRenderer>();
        origen = _rayo;
        frecuencia = _frecuencia;
        amplitud = _amplitud;
        rebotes = _rebotes;
    }


    ///
    //Dispara un rayo
    public Collider2D DispararRayo() {

        //Reflejo.RebostesRayo(3, new Vector2[] { origen.position, origen.up });
        //return null;

        //lanza un raycast y guarda el objetivo si este existe, sino es nulo
        RaycastHit2D _objetivo = Physics2D.Raycast(origen.position, origen.up);

        //verifica el objetivo tiene collider y si es interactuble(layer=Interactuable)
        if (!_objetivo.collider)
        {
            PararRayo();//apaga el rayo si este estaba prendido
            return null;//en caso de no encontrar un objetivo, cancela la funcion
        }
        //Actualiza el objetivo
        objetivo = _objetivo.collider;

        List<Vector2> _puntosImpacto = Reflejo.RebostesRayo(rebotes, new Vector2[] { origen.position, origen.up },"Reflejante",ref objetivo);
        List<Vector3> _puntosLinea = new List<Vector3>();


        for (int i = 0; i < _puntosImpacto.Count - 1; i++)
        {
            foreach (Vector3 _i in LanzarRayo(_puntosImpacto[i], _puntosImpacto[i + 1]))
                _puntosLinea.Add(_i);
        }

        rayo.positionCount = _puntosLinea.ToArray().Length;
        rayo.SetPositions(_puntosLinea.ToArray());

        rayo.enabled = true;

        
        //verifica que tipo de polaridad tiene el obejtivo
        TipoRayo();

        // devuelve el collider del objetivo
        return objetivo;
    }

    ///
    //Inicia el proceso de transferencia del personaje actual al nuevo objetivo
    public bool Transferencia() {
        if(!objetivo) return false;//si no existe objetivo, no se inicia la transferencia
        if(!esInteractuable) return false;//si no es interactuble, no se inicia la transferencia
        aumentoCarga += Time.deltaTime;//carga temporal de transferencia 
        float _tiempoPosesion = objetivo.GetComponent<Controlador_entidad>().TiempoPosesion;
        /*
        if(aumentoCarga>=_tiempoPosesion)
            return true;
        
        return false;
        */
        //float _distancia = Vector3.Distance(origen.position, objetivo.transform.position);
        //float _cargando = Mathf.Lerp(0, _distancia, aumentoCarga);

        cargaActual = aumentoCarga / _tiempoPosesion;//porcentaje de transferencia 
        //devuelve verdado cuando el la carga es del 100%
        return cargaActual >= 1;
    }
    public void CancelarTransferencia() { 
        aumentoCarga = 0;
        cargaActual = 0;
    }
    private Vector3[] LanzarRayo(Vector3 _puntoInicial, Vector3 _puntoFinal) 
    {
        

        int _distancia = Mathf.CeilToInt(Vector3.Distance(_puntoInicial,_puntoFinal)*frecuencia);//transforma la distancia del centro hacia objetivo en un entero
            
        rayo.positionCount = _distancia+1;//la cantidad de segmentos que tendra el rayo, que es igual a la distancia(int) + 1
        
        //entre segmento y segento hay puntos

        Vector3 _puntoActual = _puntoInicial;//posicion temporal del punto actual del rayo, inicia en el centro
       
        Vector3 _ruido = _puntoActual; //posicion final del punto actual del rayo, inicia en el centro

        Vector3[] _puntos = new Vector3[_distancia+1];

        for (int i = 0; i <= _distancia; i++)//actualizacion de las posiciones de los puntos del rayo
        {   
            //rayo.SetPosition(i,_ruido); //actualiza la posicion del punto i 
            _puntos[i] = _ruido;
            //_puntoActual += origen.up/frecuencia;//asigna el valor de la posicion del siguiente punto en el su eje 
            Vector3 _i = (_puntoFinal - _puntoInicial).normalized;

            _puntoActual += _i/frecuencia;

            //_ruido = _puntoActual + origen.right*Random.Range(-cargaActual,cargaActual)*amplitud;//agrega ruido a la posicion del siguiente punto
            _ruido = _puntoActual + Utilidades.VectorPerpendicular(_i)*Random.Range(-cargaActual,cargaActual)*amplitud;
        }
        _puntos[_distancia] = _puntoFinal;
        //rayo.SetPosition(_distancia,_puntoFinal);//coloca el ultimo punto en la posicion final

        //rayo.SetPositions(_puntos);
        return _puntos;
        //rayo.enabled = true; //prende en rayo
    }
    private void TipoRayo() {
        if (!objetivo.GetComponent<Administrador_entidad>() || !objetivo.GetComponent<Administrador_entidad>().interactuable)
        {
            rayo.startColor = rayo.endColor = new Color(1,1,1,0.1f);
            esInteractuable = false;
            CancelarTransferencia();
            return;
        }
        if (objetivo.GetComponent<Administrador_entidad>().polaridad)
            rayo.startColor = rayo.endColor = Color.blue;
        else
            rayo.startColor = rayo.endColor = Color.red;
        esInteractuable = true;
    }

    public void PararRayo() {
        CancelarTransferencia();
        rayo.enabled = false;
    }
}
