using UnityEngine;
using System.Collections.Generic;

namespace JuegoShock.Interaccion
{
    public struct Reflejo {

        public static List<Vector2> RebostesRayo(int _cantidadRebotesMax,Vector2[] _rayoInicial,string _tag,ref Collider2D _objetivo){
            

            List<Vector2> _puntosChoque = new List<Vector2>();

            for (int i = 0; i <= _cantidadRebotesMax;i++)
            {
                _puntosChoque.Add(_rayoInicial[0]);
                
                if (!DisparoRayo(ref _rayoInicial[0], ref _rayoInicial[1], _tag,ref _objetivo))
                {   
                    _puntosChoque.Add(_rayoInicial[0]);
                    break;
                }
                _puntosChoque.Add(_rayoInicial[0]);
            }
            
            return _puntosChoque;
        }

        private static bool DisparoRayo(ref Vector2 _origen,ref Vector2 _direccion,string _tag,ref Collider2D _obj) {

            RaycastHit2D _objetivo = Physics2D.Raycast(_origen,_direccion);//rayo 

            if (_objetivo)
            {
                _obj = _objetivo.collider;
                Vector2 _reflejo = Vector2.Reflect(_direccion,_objetivo.normal);//calcula la direccion reflejada

                _origen = _objetivo.point;
                _direccion = _reflejo;
                return _objetivo.transform.CompareTag(_tag);
            }
            return false;
        }
        
    }
}